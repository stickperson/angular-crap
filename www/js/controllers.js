angular.module('starter.controllers', [])

// A simple controller that fetches a list of leads
.controller('LeadsIndexCtrl', function($scope, $state, LeadsService) {
  // "Pets" is a service returning mock data (services.js)
  $scope.leads = LeadsService.all();
  console.log($scope.leads);
  $scope.actions = {
    add: function() {
      LeadsService.createNewLead = true;
      $state.transitionTo('lead-edit');
    },
    toggleMenu: function() {
      console.log('menu clicked');
      $scope.sideMenuController.toggleLeft();
    }
  };
})

// A simple controller that shows a tapped item's data
.controller('LeadsDetailCtrl', function($scope, $stateParams, $state, LeadsService) {
  // "Leads" is a service returning mock data (services.js)
  $scope.lead = LeadsService.get($stateParams.leadId);
  $scope.actions = {
    edit: function() {
      $state.transitionTo('lead-edit', {leadId: $scope.lead.id});
    },
    back: function() {
      $state.transitionTo('leads');
    }
  };
  $scope.fake = LeadsService.fakeApi();
})

.controller('LeadEditCtrl', function($scope, $stateParams, $state, $ionicActionSheet, LeadsService) {
  // "Leads" is a service returning mock data (services.js)
  $scope.LeadsService = LeadsService;
  if (LeadsService.createNewLead) {
    $scope.lead = {
      id: util.generateUUID()
    };
  }
  else {
    $scope.lead = LeadsService.get($stateParams.leadId);
  }
  // Copy lead for editing
  $scope.input = angular.copy($scope.lead);
  $scope.actions = {
    cancel: function() {
      if (LeadsService.createNewLead) {
        $state.transitionTo('leads');
      } else {
        $state.transitionTo('lead-detail', {leadId: $scope.lead.id});
      }
      LeadsService.createNewLead = false;
    },
    save: function() {
      LeadsService.set($scope.input);
      $state.transitionTo('lead-detail', {leadId: $scope.input.id});
      LeadsService.createNewLead = false;
    },
    remove: function() {
      LeadsService.remove($scope.lead.id);
      $state.transitionTo('leads');
    },
    confirm: function() {
      console.log($scope.lead);
      $ionicActionSheet.show({

        // The text of the red destructive button
        destructiveText: 'Delete',

        // The title text at the top
        titleText: 'Delete Lead?',

        // The text of the cancel button
        cancelText: 'Cancel',

        // Called when the sheet is cancelled, either from triggering the
        // cancel button, or tapping the backdrop, or using escape on the keyboard
        cancel: function() {
        },

        // Called when the destructive button is clicked. Return true to close the
        // action sheet. False to keep it open
        destructiveButtonClicked: function() {
          $scope.actions.remove();
          return true;
        }
      });
    }
  };
});

var util = {};
util.generateUUID = function() {
    return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
};
