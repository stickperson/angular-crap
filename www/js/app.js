// Ionic Starter App, v0.9.20

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.services', 'starter.controllers'])


.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('leads', {
      url: '/leads',
      templateUrl: '/templates/leads.html',
      controller: 'LeadsIndexCtrl'
    })

    .state('lead-detail', {
      url: '/leads/:leadId',
      templateUrl: '/templates/lead-detail.html',
      controller: 'LeadsDetailCtrl'
    })

    .state('lead-edit', {
      url: '/leads/:leadId/edit',
      templateUrl: '/templates/lead-edit.html',
      controller: 'LeadEditCtrl'
    })


    // Side menu
    .state('menu', {
      url: "/menu",
      abstract: true,
      templateUrl: "templates/side.html"
    })

    .state('menu.home', {
      url: "/home",
      views: {
        'menuContent' :{
          templateUrl: "/templates/sidemenu.html"
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/leads');

});
