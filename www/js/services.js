angular.module('starter.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('PetService', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var pets = [
    { id: 0, title: 'Cats', description: 'Furry little creatures. Obsessed with plotting assassination, but never following through on it.' },
    { id: 1, title: 'Dogs', description: 'Lovable. Loyal almost to a fault. Smarter than they let on.' },
    { id: 2, title: 'Turtles', description: 'Everyone likes turtles.' },
    { id: 3, title: 'Sharks', description: 'An advanced pet. Needs millions of gallons of salt water. Will happily eat you.' }
  ];

  return {
    all: function() {
      return pets;
    },
    get: function(petId) {
      // Simple index lookup
      return pets[petId];
    }
  };
})

.factory('LeadsService', function($http) {
  var leads = {};
  info = [['Julio', 'Rodriguez', 'Laboratory animal technician', 'Woolf Brothers', 'eric@bigidea.io', 'prospect'],
    ['Kathy', 'Russell', 'Quality control technician', 'Future Bright', 'eric@bigidea.io', 'investor'],
    ['Timothy', 'Francois', 'Transportation attendant', 'Buckeye Furniture', 'eric@bigidea.io', 'customer, prospect']];
  for (var i=0; i<info.length; i++){
    var lead = {};
    lead.id = util.generateUUID();
    lead.first = info[i][0];
    lead.last = info[i][1];
    lead.title = info[i][2];
    lead.company = info[i][3];
    lead.owner = info[i][4];
    lead.leadType = info[5];
    leads[lead.id] = lead;
  }
  return {
    all: function() {
      return leads;
    },
    get: function(leadId) {
      return leads[leadId];
    },
    set: function(lead) {
      leads[lead.id] = lead;
    },
    remove: function(leadId) {
      delete leads[leadId];
    },
    fakeApi: function() {
      console.log('getting data');
      $http.get('localhost:9000/api/v1/teams').success(function(data){
        return data;
      });
    }
  };
});

var util = {};
util.generateUUID = function() {
    return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
};
