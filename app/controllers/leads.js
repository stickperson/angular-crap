// Since we are using the Cordova SQLite plugin, initialize AngularJS only after deviceready
document.addEventListener("deviceready", function() {
  angular.bootstrap(document, ['leadsApp']);
});

var leadsApp = angular.module('leadsApp', ['LeadsModel', 'hmTouchevents']);

// Index: http://localhost/views/leads/index.html
leadsApp.controller('IndexCtrl', function ($scope, Leads) {

  // Populated by $scope.loadLeadss
  $scope.leadss = [];

  // Helper function for opening new webviews
  $scope.open = function(id) {
    webView = new steroids.views.WebView("/views/leads/show.html?id="+id);
    steroids.layers.push(webView);
  };

  $scope.loadLeadss = function() {
    $scope.loading = true;

    persistence.clean();  // Clean persistence.js cache before making a query

    // Persistence.js query for all leadss in the database
    Leads.all().list(function(leadss) {
      $scope.leadss = leadss;
      $scope.loading = false;
      $scope.$apply();
    });

  };

  // Fetch all objects from the backend (see app/models/leads.js)
  $scope.loadLeadss();

  // Get notified when an another webview modifies the data and reload
  window.addEventListener("message", function(event) {
    // reload data on message with reload status
    if (event.data.status === "reload") {
      $scope.loadLeadss();
    };
  });


  // -- Native navigation

  // Set up the navigation bar
  steroids.view.navigationBar.show("Leads index");

  // Define a button for adding a new leads
  var addButton = new steroids.buttons.NavigationBarButton();
  addButton.title = "Add";

  // Set a callback for the button's tap action...
  addButton.onTap = function() {
    var addView = new steroids.views.WebView("/views/leads/new.html");
    steroids.modal.show(addView);
  };

  // ...and finally show it on the navigation bar.
  steroids.view.navigationBar.setButtons({
    right: [addButton]
  });


});


// Show: http://localhost/views/leads/show.html?id=<id>

leadsApp.controller('ShowCtrl', function ($scope, Leads) {

  // Helper function for loading leads data with spinner
  $scope.loadLeads = function() {
    $scope.loading = true;

    persistence.clean(); // Clean persistence.js cache before making a query

    // Fetch a single object from the database
    Leads.findBy(persistence, 'id', steroids.view.params.id, function(leads) {
      $scope.leads = leads;
      $scope.loading = false;
      steroids.view.navigationBar.show(leads.name);
      $scope.$apply();
    });

  };

  // Save current leads id to localStorage (edit.html gets it from there)
  localStorage.setItem("currentLeadsId", steroids.view.params.id);

  var leads = new Leads()
  $scope.loadLeads()

  // When the data is modified in the edit.html, get notified and update (edit will be on top of this view)
  window.addEventListener("message", function(event) {
    if (event.data.status === "reload") {
      $scope.loadLeads();
    };
  });

  // -- Native navigation
  var editButton = new steroids.buttons.NavigationBarButton();
  editButton.title = "Edit";

  editButton.onTap = function() {
    webView = new steroids.views.WebView("/views/leads/edit.html");
    steroids.modal.show(webView);
  }

  steroids.view.navigationBar.setButtons({
    right: [editButton]
  });


});


// New: http://localhost/views/leads/new.html

leadsApp.controller('NewCtrl', function ($scope, Leads) {

  $scope.close = function() {
    steroids.modal.hide();
  };

  $scope.create = function(options) {
    $scope.loading = true;

    var leads = new Leads(options);

    // Add the new object to the database and then persist it with persistence.flush()
    persistence.add(leads);
    persistence.flush(function() {

      // Notify index.html to reload data
      var msg = { status: 'reload' };
      window.postMessage(msg, "*");

      $scope.close();
      $scope.loading = false;

    }, function() {
      $scope.loading = false;

      alert("Error when creating the object, is SQLite configured correctly?");

    });

  }

  $scope.leads = {};

});


// Edit: http://localhost/views/leads/edit.html

leadsApp.controller('EditCtrl', function ($scope, Leads) {

  $scope.close = function() {
    steroids.modal.hide();
  };

  $scope.update = function(options) {
    $scope.loading = true;

    var leads = new Leads(options);

    // Update the database by adding the updated object, then persist the change with persistence.flush()
    persistence.add(leads);
    persistence.flush(function() {

      window.setTimeout(function(){
        // Notify show.html below to reload data
        var msg = { status: "reload" };
        window.postMessage(msg, "*");
        $scope.close();
      }, 1000);

      $scope.loading = false;

    });

  };

  // Helper function for loading leads data with spinner
  $scope.loadLeads = function() {
    $scope.loading = true;

    var id  = localStorage.getItem("currentLeadsId");

    // Fetch a single object from the database
    Leads.findBy(persistence, 'id', id, function(leads) {
      $scope.leads = leads;
      $scope.loading = false;

      $scope.$apply();
    });
  };

  $scope.loadLeads();

});